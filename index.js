const express = require('express');

const app = express();
app.use(express.json());

app.get('/', (req, res) => {
  res.json({ message: 'Hello Dev' }).status(2000);
});

app.listen(3000, console.log(`is running port 3000`));
